<?php declare(strict_types = 1);

$injector = new \Auryn\Injector;

$injector->alias('Http\Request', 'Http\HttpRequest');
$injector->share('Http\HttpRequest');
$injector->define('Http\HttpRequest', [
    ':get' => $_GET,
    ':post' => $_POST,
    ':cookies' => $_COOKIE,
    ':files' => $_FILES,
    ':server' => $_SERVER,
]);

$injector->alias('Http\Response', 'Http\HttpResponse');
$injector->share('Http\HttpResponse');

$injector->alias('Insta\App\Core\Template\Renderer', 'Insta\App\Core\Template\TwigRenderer');



$injector->alias('Insta\App\Core\Template\FrontendRenderer', 'Insta\App\Core\Template\FrontendTwigRenderer');
$injector->share('Insta\App\Core\Session');
$injector->delegate('Twig_Environment', function() use ($injector) {
    $loader = new \Twig_Loader_Filesystem(__DIR__ . '/App/templates');
    $twig = new \Twig_Environment($loader, [
        'debug' => true,
    ]);
    $twig->addExtension(new \Twig_Extension_Debug());
    return $twig;
});

return $injector;