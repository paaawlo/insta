<?php

namespace Insta\App\Models;

use Insta\App\Core\Database;
use Insta\App\Core\Model;

class Role extends Model
{
    /** @var string */
    protected $tableName = 'role';

    /** @var int */
    protected $id = 0;

    /** @var  string */
    protected $name;

    /**
     * User constructor.
     * @param Database $database
     */
    public function __construct(Database $database)
    {
        parent::__construct($database);
    }

}