<?php

namespace Insta\App\Models;

use Insta\App\Core\Database;
use Insta\App\Core\Model;

/**
 * Class User
 * @package Insta\App\Models
 */
class User extends Model
{
    /** @var string */
    protected $tableName = 'user';

    /** @var int */
    protected $id = 0;

    /** @var  string */
    protected $name;

    /** @var  string */
    protected $email;

    /** @var  string */
    protected $password;

    /** @var  int */
    protected $roleId;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return int
     */
    public function getRoleId()
    {
        return $this->roleId;
    }

    /**
     * @param int $roleId
     */
    public function setRoleId($roleId)
    {
        $this->roleId = $roleId;
    }

    /**
     * User constructor.
     * @param Database $database
     */
    public function __construct(Database $database)
    {
        parent::__construct($database);
    }

}