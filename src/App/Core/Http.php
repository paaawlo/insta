<?php

namespace Insta\App\Core;

use Http\HttpRequest;
use Http\HttpResponse;

/**
 * Class Http
 * @package Insta\App\Core
 */
class Http
{
    /**
     * @var HttpResponse
     */
    private $response;

    /**
     * @var HttpRequest
     */
    private $request;

    /**
     * Http constructor.
     * @param HttpRequest $request
     * @param HttpResponse $response
     */
    public function __construct(HttpRequest $request, HttpResponse $response)
    {
        $this->request = $request;
        $this->response = $response;
    }

    /**
     * @return HttpResponse
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @return HttpRequest
     */
    public function getRequest()
    {
        return $this->request;
    }
}