<?php

namespace Insta\App\Core;

use Insta\App\Core\Template\TwigRenderer;

class App
{
    /**
     * @var \Insta\App\Core\Session
     */
    private $session;

    /**
     * @var TwigRenderer
     */
    private $twigRenderer;

    public function __construct(Session $session, TwigRenderer $twigRenderer)
    {
        $this->session = $session;
        $this->twigRenderer = $twigRenderer;
    }

    /**
     * @return Session
     */
    public function session()
    {
        return $this->session;
    }

    /**
     * @return TwigRenderer
     */
    public function twigRenderer()
    {
        return $this->twigRenderer;
    }
}