<?php

namespace Insta\App\Core;

use PDO;

/**
 * Class Database
 */
class Database
{

    protected $connection;
    private $username;
    private $password;
    private $host;
    private $dbname;
    private $options;
    private static $instance;

    /**
     * @return Database
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new Database();
        }
        return self::$instance;
    }

    /**
     * @return mixed
     */
    public function getConnection()
    {
        return $this->connection;
    }

    /**
     * Connection constructor
     */
    public function __construct()
    {
        $this->username = "root";
        $this->password = "root";
        $this->host = "localhost";
        $this->dbname = "insta";
        $this->connect();
    }

    /**
     * Close connection
     */
    public function __destruct()
    {
        $this->disconnect();
    }

    /**
     * Connect with database
     * @throws \Exception
     */
    public function connect()
    {
        try {
            $this->connection = new PDO("mysql:host={$this->host};dbname={$this->dbname};charset=utf8", $this->username, $this->password, $this->options);
            //get an error if unable to connect db
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->connection->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        } catch (\PDOException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * Disconnects with database
     */
    public function disconnect()
    {
        $this->connection = NULL;
    }

    /**
     * Join two tables
     * @param $column
     * @param $table
     * @param $table2
     * @param string $condition
     * @param array $params
     * @return mixed
     * @throws \Exception
     */
    public function join($column, $table, $table2, $condition = "", $params = [])
    {
        $query = "SELECT $column FROM $table JOIN $table2 ON ";
        if ($condition) {
            $query .= $condition;
        }
        try {
            $stmt = $this->connection->prepare($query);
            $stmt->execute($params);
            return $stmt->fetch();
        } catch (\PDOException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * Get single row from database
     * @param $column
     * @param $table
     * @param string $condition
     * @return mixed
     * @throws \Exception
     */
    public function getRow($column, $table, $condition = "")
    {
        $query = "SELECT $column FROM $table ";
        if ($condition) {
            $query .= $condition;
        }
        try {
            $stmt = $this->connection->prepare($query);
            $stmt->execute();
            return $stmt->fetch();
        } catch (\PDOException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * Create new row
     * @param $table
     * @param $condition
     * @return mixed
     * @throws \Exception
     */
    public function insertRow($table, $condition)
    {
        $query = "INSERT INTO $table $condition";
        try {
            $stmt = $this->connection->prepare($query);
            $stmt->execute();
            $lastId = $this->connection->lastInsertId();
            return $lastId;
        } catch (\PDOException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * Update row
     * @param $table
     * @param $condition
     * @return bool
     * @throws \Exception
     */
      public function updateRow($table, $condition)
    {
        $query = "UPDATE $table SET $condition";
        try {
            $stmt = $this->connection->prepare($query);
            $stmt->execute();
            return TRUE;
        } catch (\PDOException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * Delete row
     * @param $table
     * @param string $condition
     * @throws \Exception
     */
    public function deleteRow($table, $condition = "")
    {
        $query = "DELETE FROM $table ";
        if ($condition) {
            $query .= $condition;
        }
        try {
            $stmt = $this->connection->prepare($query);
            $stmt->execute();
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

}