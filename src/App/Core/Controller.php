<?php declare(strict_types=1);

namespace Insta\App\Core;

abstract class Controller
{
    /**
     * @var App
     */
    protected $app;

    /**
     * @var Http
     */
    protected $http;

    /**
     * Controller constructor.
     * @param App $app
     * @param Http $http
     */
    protected function __construct(App $app, Http $http)
    {
        $this->app = $app;
        $this->http = $http;
    }
}