<?php

namespace Insta\App\Core;

/**
 * Class Session
 * @package Insta\Lib\Helpers
 */
class Session
{
    /**
     * @var boolean
     */
    private $session;

    /**
     * Start session
     */
    public function start()
    {
        if ($this->session == false) {
            if (session_id() === "") {
                session_start();
                $this->session = true;
            }
        }
    }

    /**
     * Get session value by key
     * @param string $key
     * @return mixed
     */
    public function get(string $key)
    {
        if (isset($_SESSION[$key])) {
            return $_SESSION[$key];
        }
    }

    /**
     * Set value to session key
     * @param string $key
     * @param $value
     */
    public function set(string $key, $value)
    {
        $_SESSION[$key] = $value;
    }

    /**
     * Unset session key
     * @param string $key
     */
    public function unset(string $key)
    {
        unset($_SESSION[$key]);
    }

    /**
     * Destroy session
     */
    public function destroy()
    {
        if ($this->session == true) {
            session_unset();
            session_destroy();
        }
    }
}