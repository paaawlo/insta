<?php declare(strict_types = 1);

namespace Insta\App\Core\Template;

use Twig_Environment;

class TwigRenderer implements Renderer
{
    private $renderer;

    public function __construct(Twig_Environment $renderer)
    {
        $this->renderer = $renderer;
    }

    public function render(string $template, array $data = []) : string
    {
        return $this->renderer->render($template, $data);
    }
}