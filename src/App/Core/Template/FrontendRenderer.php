<?php declare(strict_types = 1);

namespace Insta\App\Core\Template;

interface FrontendRenderer extends Renderer {}