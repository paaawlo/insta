<?php declare(strict_types = 1);

namespace Insta\App\Core\Template;

/**
 * Interface Renderer
 * @package Insta\Template
 */
interface Renderer
{
    /**
     * @param string $template
     * @param array $data
     * @return string
     */
    public function render(string $template, array $data = []) : string;
}