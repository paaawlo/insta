<?php

namespace Insta\App\Core;

/**
 * Class Model
 * @package Insta\App\Core
 */
abstract class Model
{
    /** @var string */
    protected $tableName = "";

    /** @var Database */
    protected $database;

    /**
     * Model constructor.
     * @param Database $database
     */
    public function __construct(Database $database)
    {
        $this->database = $database::getInstance();
    }

    /**
     * Save new model/update existing
     */
    public function save()
    {
        $modelVariables = get_object_vars($this);
        $values = "";
        $columns = [];

        foreach ($modelVariables as $key => $value) {
            if ($key !== 'database' && $key !== 'tableName') {
                $columns[] = strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $key));;
                $values[] .= "'" . $value . "'";
            }
        }
        $columnsAndValues = array_combine($columns, $values);
        $columns = implode(',', $columns);
        $values = implode(',', $values);
        if ($this->id === 0) {
            $this->database->insertRow($this->tableName, "(" . $columns . ") VALUES(" . $values . ")");
            return;
        }

        $condition = "";
        foreach ($columnsAndValues as $column => $value) {
            if ($column !== 'id') {
                $condition .= "{$column}={$value},";
            }
        }
        $condition = rtrim($condition, ",");
        $condition .= " where id = {$this->id}";
        $this->database->updateRow($this->tableName, $condition);
    }

    /**
     * Load model by id
     * @param $id
     * @return $this
     */
    public function load(int $id)
    {
        $result = $this->database->getRow('*', $this->tableName, "where id = {$id}");
        if ($result) {
            foreach ($result as $key => $value) {
                if ($key !== 'database' && $key !== 'tableName') {
                    $this->{$key} = $value;
                }
            }
            return $this;
        }
    }

    /**
     * Load model by custom column
     * @param string $columnName
     * @param $value
     * @return $this
     */
    public function loadBy(string $columnName, $value)
    {
        $result = $this->database->getRow('*', $this->tableName, "where {$columnName} = '{$value}'");
        if ($result) {
            foreach ($result as $key => $value) {
                if ($key !== 'database' && $key !== 'tableName') {
                    $this->{$key} = $value;
                }
            }
            return $this;
        }
    }

    /**
     * Delete row
     * @param $id
     */
    public function delete($id = '')
    {
        if ($this->id !== 0) {
            $this->database->deleteRow($this->tableName, "where id = {$this->id}");
            return;
        }
        if ($this->load($id)) {
            $this->database->deleteRow($this->tableName, "where id = {$id}");
        }
    }
}