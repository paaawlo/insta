<?php declare(strict_types=1);

namespace Insta\App\Controllers;

use Insta\App\Core\Controller;
use Insta\App\Core\Http;
use Insta\App\Core\App;
use Insta\App\Models\User;

class Register extends Controller
{
    CONST USER_ROLE_ID = 3;

    /** @var User */
    private $userModel;

    /**
     * Register constructor.
     * @param App $app
     * @param Http $http
     * @param User $user
     */
    public function __construct(App $app, Http $http, User $user)
    {
        parent::__construct($app, $http);
        $this->userModel = $user;
    }

    /**
     * User register form
     */
    public function form()
    {

        $data = [
            'name' => $this->http->getRequest()->getParameter('name', 'stranger'),
            'menuItems' => [['href' => '/', 'text' => 'Homepage']],
        ];
        $html = $this->app->twigRenderer()->render('home.twig', $data);
        $this->http->getResponse()->setContent($html);
    }

    /**
     * Register new user
     */
    public function register()
    {
        $params = $this->http->getRequest()->getParameters();

        $this->userModel->setName($params['username']);
        $this->userModel->setEmail($params['email']);
        $this->userModel->setPassword((password_hash($params['password'], PASSWORD_DEFAULT, ['cost' => 10])));
        $this->userModel->setRoleId(self::USER_ROLE_ID);
        $this->userModel->save();
    }


    public function emailValidation()
    {
        $email = $this->http->getRequest()->getParameter('email');
        $this->userModel->loadBy('email', $email);
        if($this->userModel->getEmail() !== null){
            echo $this->userModel->getEmail();
        }
    }
}