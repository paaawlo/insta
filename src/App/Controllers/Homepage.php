<?php declare(strict_types=1);

namespace Insta\App\Controllers;

use Insta\App\Core\Controller;
use Insta\App\Core\Http;
use Insta\App\Core\App;
use Insta\App\Models\User;

class Homepage extends Controller
{
    private $userModel;

    public function __construct(App $session, Http $http, User $user)
    {
        parent::__construct($session, $http);
        $this->userModel = $user;
    }

    public function show()
    {
        $user = $this->userModel->load(6);
        $user->delete();


        $this->app->session()->set('age', '25');
        //$html = $this->frontendRenderer->render('', $data);
        $x = 'asd';
        $this->http->response()->setContent($this->app->session()->get('age'));
    }
}