<?php declare(strict_types = 1);

return [
    ['GET', '/', ['Insta\App\Controllers\Register', 'form']],
    ['POST', '/register', ['Insta\App\Controllers\Register', 'register']],
    ['POST', '/emailValidation', ['Insta\App\Controllers\Register', 'emailValidation']],
];