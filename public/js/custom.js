// register form validation //
$(document).ready(function () {

    $('#account-register-form').validate({ // initialize the plugin
        rules: {
            username: {
                minlength: 5,
                maxlength: 20,
                required: true
            },
            email: {
                required: true,
                email: true
            },
            password: {
                minlength: 5,
                maxlength: 20
            },
            password_confirm: {
                equalTo: "#password"
            }
        },
        messages: {
            username: {
                required: 'Enter username',
                minlength: 'Minimum length is 5 characters',
                maxlength: 'Maximum length is 20 characters'
            },
            email: {
                required: 'Enter email',
                email: 'Please enter valid email'
            },
            password: {
                minlength: 'Minimum length is 5 characters',
                maxlength: 'Maximum length is 20 characters'
            },
            password_confirm: {
                equalTo: "Password and password confirmation does not match"
            }
        }
    });

});


$("#account-register-button").on('click', function () {
    $.ajax({
        type: "POST",
        url: "emailValidation",
        data: $("#account-register-form").serialize(),
        success: function (e) {
            if (e) {
                $("#legend").append('<div>email exists</div>');
            } else {
                $.ajax({
                    type: "POST",
                    url: "register",
                    data: $("#account-register-form").serialize(),
                    success: function () {
                        console.log('account created');
                    }
                });
            }

        }
    });
});